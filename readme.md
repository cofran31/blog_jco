<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Proyecto ORM BLOG (JUAN CARLOS ORTUBE LAHOR)

Se desarrollo la aplicacion BLog 

Primeramente se realizo las migraciones de las tablas:

<p align="center"><img src="http://200.107.241.6/img_orm/mer.png"></p>

Seguidamente se desarrollo los modelos de la aplicacion:

<p align="center"><img src="http://200.107.241.6/img_orm/modelos.png"></p>

Luego se realizo los Factorys:

<p align="center"><img src="http://200.107.241.6/img_orm/factory.png"></p>

Seguidamente se crea los Seeder para los datos FAKE:

<p align="center"><img src="http://200.107.241.6/img_orm/seeder.png"></p>

Se ejecuto para el llenado de datos con datos FAKE:

<p align="center"><img src="http://200.107.241.6/img_orm/tabla_users.png"></p>
<p align="center"><img src="http://200.107.241.6/img_orm/tabla_roles.png"></p>
<p align="center"><img src="http://200.107.241.6/img_orm/tabla_publishes.png"></p>
<p align="center"><img src="http://200.107.241.6/img_orm/tabla_post.png"></p>
<p align="center"><img src="http://200.107.241.6/img_orm/tabla_photos.png"></p>
<p align="center"><img src="http://200.107.241.6/img_orm/tabla_comments.png"></p>

A continuacion verificamos el mapeo de datos:

<p align="center"><img src="http://200.107.241.6/img_orm/mapeo_users.png"></p>
<p align="center"><img src="http://200.107.241.6/img_orm/mapero_comment.png"></p>

Se realizo la configuracion de roles para el uso de administador y usuario se verifica la creacion de las tablas y el inicio en HOME

<p align="center"><img src="http://200.107.241.6/img_orm/tablas_finales.png"></p>
<p align="center"><img src="http://200.107.241.6/img_orm/home.png"></p>

La configuracion del archivo .ENV se encuentra en el archivo .env.txt








