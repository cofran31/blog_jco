<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Publish extends Model {

    protected $fillable = [
        'slug',
        'label', 
        'is_publish'
    ];
    public function users()
    {
        return $this->belongsToMany(User::class,'posts','autor_id','publishes_id')
            ->withPivot('published_at', 'detele_at')
    	    ->withTimestamps();
    }

}
