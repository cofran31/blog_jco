<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model {

    protected $fillable = [
        'filename', 
        'type'
    ];

    public function post() {
        return $this->belongsToMany(Post::class, 'photo_post', 'photos_id', 'posts_id')
                        ->withPivot('use', 'order')
                        ->withTimestamps();
    }

}
