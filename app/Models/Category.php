<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories"; 
    
    protected $fillabel = [
        'id'
    ];
    
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
   // protected $appends = [
     //   "Nombre Lenguaje"
   // ];
    public function language()
    {
        return $this->belongsToMany(Language::class,'category_language','categories_id','languages_id')
            ->withPivot('label', 'slug','description')
    	    ->withTimestamps();
        
    } 

    
}
