<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model {

    protected $fillable = [
        'label', 
        'iso6391'
    ];

    public function post() {
        return $this->belongsToMany(Post::class, 'language_post', 'languages_id', 'posts_id')
                        ->withPivot('title', 'slug', 'content')
                        ->withTimestamps();
    }

    public function category() {
        return $this->belongsToMany(Category::class, 'category_language', 'languages_id', 'categories_id')
                        ->withPivot('label', 'slug', 'content')
                        ->withTimestamps();
    }

}
