<?php
use App\User;
use App\Models\Post;
use App\Models\Publish;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Language;
use App\Models\Photo;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('users', function () {
    return User::get();
});
Route::get('users/{users_id}', function ($nombre) {
    return User::where("first_name", "=",$nombre)->first();
});
Route::get('posts', function () {
    return Post::get();
});
Route::get('photo', function () {
    return Photo::get();
});
Route::get('language', function () {
    return Language::get();
});
Route::get('comment', function () {
    return Comment::get();
});
Route::get('category', function () {
    return Category::get();
});
Route::get('posts/{posts_id}', function ($posts_id) {
    return Post::with("photos")->find($posts_id);
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
