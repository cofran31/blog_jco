<?php

use Illuminate\Database\Seeder;
use App\Models\Photo;
use App\Models\Post;

class PhotoTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker\Factory::create();
        $allPosts = Post::get();
        factory(Photo::class, 30)->create()->each(function($photo) use ($allPosts, $faker) {
            $random_p = $allPosts->random(rand(0, 9));
            for ($i = 0; $i < count($random_p); $i++) {
                $registro[] = array(
                    'posts_id' => $random_p[$i]['id'],
                    'use' => $faker->word(),
                    'order' => ++$i,
                );
            }
            $photo->posts()->sync($registro);
        });
    }

}
