<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Post;
use App\Models\Publish;
use App\User;

class PostTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $faker = Faker::create();
        foreach (range(1,30) as $index) {
            DB::table('posts')->insert([
                'autor_id' => $faker->randomElement(User::pluck('id')->toArray()),
            	'publishes_id' => $faker->randomElement(Publish::pluck('id')->toArray()),
                'published_at' => $faker->dateTime($max = 'now'),
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now'),
            ]);
        }
    }

}
