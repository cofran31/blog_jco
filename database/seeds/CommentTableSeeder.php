<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
use App\Models\Post;

class CommentTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker::create();
        foreach (range(0, 99) as $index) {
            DB::table('comments')->insert([
                'autor_id' => $faker->randomElement(User::pluck('id')->toArray()),
                'comment' => $faker->text(),
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now'),
                'posts_id' => $faker->randomElement(Post::pluck('id')->toArray()),
            ]);
        }
    }

}
