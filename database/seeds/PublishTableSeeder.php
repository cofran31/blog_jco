<?php
use Illuminate\Database\Seeder;
use App\User;
use App\Models\Publish;

class PublishTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $allAutor = User::get();
        factory(Publish::class, 100)->create()->each(function($publish) use ($allAutor) {
            $random = $allAutor->random(0,99);
            $publish->users()->sync($random);
        });
    }

}
