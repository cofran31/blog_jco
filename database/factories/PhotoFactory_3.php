<?php

use Faker\Generator as Faker;
use App\Models\Photo;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Photo::class, function (Faker $faker) {
     $types = [
        "png"
    ];
        return [
        'filename' =>$faker->unique()->image('public/img',50,50, null, false), 
        'type'=> $types [array_rand($types,1)]
    ];

});
